// set up ========================
    var express  = require('express');
    var app      = express();                              // create our app w/ express 
          // get instance of mysql
    var morgan = require('morgan');             // log requests to the console (express4)
    var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
    var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
    var path = require('path');
    var cookieParser = require('cookie-parser');
    var session = require('express-session');

    // configuration =================
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'jade');
    app.use(express.static(__dirname + '/public/'));                 // set the static files location /public/img will be /img for users
    app.use(morgan('dev'));                                         // log every request to the console
    app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
    app.use(bodyParser.json());                                     // parse application/json
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    app.use(methodOverride());
    
    app.use(cookieParser());
    app.use(session({ secret: 'randomsecretkeyforthisapp' }));
    
    //  Read more about mysql connection here : https://www.npmjs.com/package/mysql
    var pool= require('./app/config/database')();
    var con; 
    pool.on('connection', function (connection) {
        con=connection;
        console.log('from server : connected as id' + connection.threadId);
        // Intitialising routes
        require('./app/routes')(app,con);
    });
    app.listen(process.env.PORT || 8080);
    console.log("App listening on port 8080");