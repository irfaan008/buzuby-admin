/**
*  PaymentBlockKeywords schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.053Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  PaymentBlockKeywords Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var PaymentBlockKeywords = schema.define('payment_block_keywords', {
         txn_id: { type: schema.String, limit: 50 },
         payer_id: { type: schema.String, limit: 50 },
         payment_date: { type: schema.String, limit: 20 },
         next_payment_date: { type: schema.String, limit: 20 },
         amount: { type: schema.String },
         status: { type: schema.String, limit: 20 },
         mode_of_pay: { type: schema.String, limit: 50 },
         user_id: { type: schema.Number, limit: 11 },
         created: { type: schema.String, limit: 20 },
         modified: { type: schema.String, limit: 20 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return PaymentBlockKeywords;
};
