/**
*  UserSubscribedPlans schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.054Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  UserSubscribedPlans Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var UserSubscribedPlans = schema.define('user_subscribed_plans', {
         subscription_plan_id: { type: schema.Number, limit: 11 },
         user_id: { type: schema.Number, limit: 11 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return UserSubscribedPlans;
};
