/**
*  Payment schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.053Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  Payment Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var Payment = schema.define('payment', {
         payment_id: { type: schema.String, limit: 255 },
         pay_id: { type: schema.String, limit: 255 },
         payment_date: { type: schema.String, limit: 15 },
         amount: { type: schema.String, limit: 11 },
         status: { type: schema.String, limit: 11 },
         plan_name: { type: schema.String, limit: 20 },
         mode_of_pay: { type: schema.String, limit: 20 },
         user_id: { type: schema.Number, limit: 11 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return Payment;
};
