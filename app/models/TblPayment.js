/**
*  TblPayment schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.054Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  TblPayment Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var TblPayment = schema.define('tbl_payment', {
         payment_pk: { type: schema.Number, limit: 255 },
         payment_for_fk: { type: schema.Number, limit: 11 },
         m_payment_id: { type: schema.String, limit: 100 },
         pf_payment_id: { type: schema.String, limit: 100 },
         payment_status: { type: schema.String, limit: 100 },
         item_name: { type: schema.Text },
         item_description: { type: schema.Text },
         amount_gross: { type: schema.String },
         amount_fee: { type: schema.String },
         amount_net: { type: schema.String },
         date: { type: schema.Date },
         created_by_fk: { type: schema.Number, limit: 255 },
         created_dtm: { type: schema.Date }
    },{
         primaryKeys: ['payment_pk']

    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return TblPayment;
};
