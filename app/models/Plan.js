/**
*  Plan schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.053Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  Plan Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var Plan = schema.define('plan', {
         plan_name: { type: schema.String, limit: 255 },
         plan_min_keywords: { type: schema.Number, limit: 2 },
         plan_max_keywords: { type: schema.Number, limit: 2 },
         plan_block: { type: schema.Number, limit: 2 },
         user_id: { type: schema.Number, limit: 11 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return Plan;
};
