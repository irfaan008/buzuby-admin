/**
*  PaymentBlockKeywordDetails schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.053Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  PaymentBlockKeywordDetails Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var PaymentBlockKeywordDetails = schema.define('payment_block_keyword_details', {
         payment_block_keyword_id: { type: schema.Number, limit: 11 },
         type: { type: schema.String, limit: 20 },
         amount: { type: schema.String },
         value: { type: schema.Text }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return PaymentBlockKeywordDetails;
};
