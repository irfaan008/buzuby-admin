/**
*  Event schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.053Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  Event Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var Event = schema.define('event', {
         title: { type: schema.String, limit: 255 },
         description: { type: schema.Text },
         price: { type: schema.String },
         image_path: { type: schema.String, limit: 255 },
         category_id: { type: schema.Number, limit: 11 },
         sub_category_id: { type: schema.Number, limit: 11 },
         sub_sub_category_id: { type: schema.Number, limit: 11 },
         business_id: { type: schema.Number, limit: 11 },
         scroll_text: { type: schema.String, limit: 255 },
         keywords: { type: schema.String, limit: 500 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return Event;
};
