/**
*  ResetPasswordRequest schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.053Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  ResetPasswordRequest Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var ResetPasswordRequest = schema.define('reset_password_request', {
         user_id: { type: schema.Number, limit: 11, index : true },
         requested_email: { type: schema.String, limit: 200 },
         username: { type: schema.String, limit: 200 },
         status: { type: schema.String, limit: 100 },
         token: { type: schema.String, limit: 1000 },
         created_at: { type: schema.Date },
         used_at: { type: schema.Date }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return ResetPasswordRequest;
};
