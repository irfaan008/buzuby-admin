/**
*  BusinessRating schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.052Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  BusinessRating Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var BusinessRating = schema.define('business_rating', {
         customer_id: { type: schema.Number, limit: 11 },
         business_id: { type: schema.Number, limit: 11 },
         rating: { type: schema.Number, limit: 10 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return BusinessRating;
};
