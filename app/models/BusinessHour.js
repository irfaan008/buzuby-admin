/**
*  BusinessHour schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.052Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  BusinessHour Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var BusinessHour = schema.define('business_hour', {
         day: { type: schema.String, limit: 50 },
         start_time: { type: schema.String, limit: 100 },
         end_time: { type: schema.String, limit: 100 },
         business_id: { type: schema.Number, limit: 11 }
    },{
         indexes: {
            FK_business_hour: {
                columns: 'business_id'
            }
         }
    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return BusinessHour;
};
