/**
*  Customer schema
*
*  @version     0.0.1
*  @created     2016-03-20T01:57:12.886Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  Customer Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    // console.log('schema from model: '+schema);
    var Customer = schema.define('customer', {
         first_name: { type: schema.String, limit: 255 },
         surname: { type: schema.String, limit: 255 },
         mobile_number: { type: schema.String, limit: 20 },
         date_of_birth: { type: schema.String, limit: 50 },
         email: { type: schema.String, limit: 200 },
         address: { type: schema.String, limit: 255 },
         country: { type: schema.String, limit: 255 },
         city: { type: schema.String, limit: 255 },
         province: { type: schema.String, limit: 255 },
         suburb: { type: schema.String, limit: 255 },
         latitude: { type: schema.String },
         longitude: { type: schema.String },
         user_id: { type: schema.Number, limit: 11 },
         social_id: { type: schema.String, limit: 255 },
         active: { type: schema.Boolean, limit: 1 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return Customer;
};

