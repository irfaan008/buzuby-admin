/**
*  RestaurantType schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.054Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  RestaurantType Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var RestaurantType = schema.define('restaurant_type', {
         name: { type: schema.String, limit: 300 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return RestaurantType;
};
