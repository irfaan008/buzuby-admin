/**
*  Business schema
*
*  @version     0.0.1
*  @created     2016-03-21T22:15:17.394Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  Merchant Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var Merchant = schema.define('business', {
         name: { type: schema.String, limit: 255 },
         address: { type: schema.String, limit: 255 },
         latitude: { type: schema.String },
         longitude: { type: schema.String },
         country: { type: schema.String, limit: 200 },
         province: { type: schema.String, limit: 200 },
         suburb: { type: schema.String, limit: 200 },
         city: { type: schema.String, limit: 200 },
         phone: { type: schema.String, limit: 100 },
         restaurant_id: { type: schema.Number, limit: 11 },
         trading_hours_from: { type: schema.Number, limit: 11 },
         trading_hours_to: { type: schema.Number, limit: 11 },
         logo_img_path: { type: schema.String, limit: 200 },
         banner_img_path: { type: schema.String, limit: 200 },
         description: { type: schema.Text },
         website_link: { type: schema.String, limit: 200 },
         specialize_in: { type: schema.Text },
         facebook_page: { type: schema.String, limit: 255 },
         amenities: { type: schema.Text },
         price_range_from: { type: schema.String },
         price_range_to: { type: schema.String },
         business_facebook_url: { type: schema.String, limit: 200 },
         category_id: { type: schema.Number, limit: 11 },
         sub_category_id: { type: schema.Number, limit: 11 },
         sub_sub_category_id: { type: schema.Number, limit: 11 },
         account_number: { type: schema.String, limit: 200 },
         user_id: { type: schema.Number, limit: 11 },
         active: { type: schema.Boolean, limit: 1 },
         payment_status: { type: schema.String, limit: 50 },
         rating: { type: schema.String },
         currency: { type: schema.String, limit: 100 },
         serial_no: { type: schema.String, limit: 50 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return Merchant;
};
