/**
*  SubSubCategory schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.054Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  SubSubCategory Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var SubSubCategory = schema.define('sub_sub_category', {
         name: { type: schema.String, limit: 100 },
         sub_category_id: { type: schema.Number, limit: 11 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return SubSubCategory;
};
