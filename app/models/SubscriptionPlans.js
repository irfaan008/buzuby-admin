/**
*  SubscriptionPlans schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.054Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  SubscriptionPlans Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var SubscriptionPlans = schema.define('subscription_plans', {
         name: { type: schema.String, limit: 255 },
         keywords: { type: schema.String, limit: 255 },
         blocks: { type: schema.String, limit: 255 },
         price: { type: schema.String },
         max_keywords: { type: schema.String, limit: 255 },
         max_deals: { type: schema.String, limit: 255 },
         max_events: { type: schema.String, limit: 255 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return SubscriptionPlans;
};
