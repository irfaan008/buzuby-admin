/**
*  Keywords schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.053Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  Keywords Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var Keywords = schema.define('keywords', {
         entity_id: { type: schema.Number, limit: 11 },
         entity_type: { type: schema.String, limit: 255 },
         keyword: { type: schema.String, limit: 255 },
         sub_category_id: { type: schema.Number, limit: 11 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return Keywords;
};
