/**
*  UsersAuthCodes schema
*
*  @version     0.0.1
*  @created     2016-04-15T13:15:33.054Z
*  @link        https://camintejs.com/
*  @wiki        https://github.com/biggora/caminte/wiki
*
*  Created by create-model script
**/

/**
*  Define  UsersAuthCodes Model
*  @param  {Object}  schema
*  @return {Object}  model
*
*  @wiki   https://github.com/biggora/caminte/wiki/Defining-a-Model
**/
module.exports = function(schema){
    var UsersAuthCodes = schema.define('users_auth_codes', {
         user_id: { type: schema.Number, limit: 11 },
         token: { type: schema.String, limit: 255 }
    },{


    });

    /**
    *  Define any custom method
    *  or setup validations here
    **/

    return UsersAuthCodes;
};
