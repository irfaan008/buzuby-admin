
module.exports = function(app,con) {
    
    // require('./api/util.api')(app);
    require('./api/auth.api')(app,con);
    require('./api/customer.api')(app,con);
    require('./api/merchant.api')(app,con);
    require('./api/subscription.api')(app,con);
    require('./api/payment.api')(app,con);
    
    
    app.get('/',  checkAuth, function(req, res){
    // app.get('/', function(req, res){
        res.render('index');
    }); 
    
    app.get('/admin/',function(req, res){
        res.render('admin', {title : 'Buzuby-Login'});
    }); 
    
     function checkAuth(req, res, next) {
        if (req.session.user!='admin') {
            res.redirect('/admin/');
        } else {
            // Tell browser not to cache restricated pages
            res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
            next();
        }
    }
    function getErrorJson(code, message, extra){
        return '{"error": {"code": "'+code+'","message": "'+message+'","extra": "'+extra+'"}}';
    }
};