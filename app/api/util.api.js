module.exports = function(app) {
    
    function getErrorJson(code, message, extra){
        return '{"error": {"code": "'+code+'","message": "'+message+'","extra": "'+extra+'"}}';
    }
}