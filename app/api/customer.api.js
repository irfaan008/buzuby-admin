module.exports = function(app,con) {
    app.get('/api/customers', function(req, res) {
        // console.log('from routes : connected as id ' + con.threadId);
        con.query('SELECT * FROM customer WHERE ACTIVE=1',function(err,rows){
            if (!err)
                res.json(rows)
            else
                res.send(err);
        });
    });
    
    app.delete('/api/customers/:id', function(req, res){
        // console.log('from routes customer delete : connected as id ' + con.threadId);
        con.query('UPDATE customer SET ACTIVE=0 WHERE USER_ID=?',[req.params.id],function(err,rows){
            if (!err){
                console.log("customer with id : "+req.params.id+" deleted")
                res.json(rows.affectedRows)
            }
            else{
                console.log("Error while updating customer : "+err);
                res.send(err);
            }
        });
    });
}