module.exports = function(app,con) {
    app.get('/api/merchants', function(req, res) {
        con.query('SELECT user_id, name, phone,specialize_in,city,country,payment_status,rating,latitude,longitude FROM business WHERE ACTIVE=1',function(err,rows){
            if (!err)
                res.json(rows)
            else
                res.send(err);
        });
    });
    
    app.delete('/api/merchants/:id', function(req, res){
        con.query('UPDATE business SET ACTIVE=0 WHERE USER_ID=?',[req.params.id],function(err,rows){
            if (!err){
                console.log("merchant with id : "+req.params.id+" deleted")
                res.json(rows.affectedRows)
            }
            else{
                console.log("Error while updating merchant : "+err);
                res.send(err);
            }
        });
    });
    
    app.get('/api/merchants/subscription', function(req, res) {
        con.query('select b.name, b.user_id merchantId,s.name planName, s.price paidAmount,s.max_deals,s.max_events,s.max_keywords,s.duration Months from business b left join user_subscribed_plans s on b.user_id=s.user_id where b.active=1',function(err,rows){
            if (!err)
                res.json(rows)
            else
                res.send(err);
        });
    });
}