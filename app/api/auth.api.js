module.exports = function(app,con) {
    
     app.post('/api/login', function(req, res) {
        console.log("Api login called");
        authenticate(req.body.username, req.body.password, function(err, user) {
            console.log("api login authentication called : "+req.body.username);
            if(user){
                req.session.regenerate(function() {
                    console.log("Logged in as an "+user);
                   req.session.user=user;
                   req.session.status = 'Authenticated as '+user;
                   res.render('index'); 
                });
            } else{
                console.log("Authentication failed : "+err);
                req.session.error='Authentication failed. Please check your username and password';
                res.header("Content-Type","text/json");
                res.send(getErrorJson(100,"Incorrect credentials","Not able to authenticate user"));
               
            }
        });
    });
    
     app.get('/api/logout', function(req, res) {
        console.log("Api logout called");
        req.session.user="";
        res.json("success");
    });
    
    function getErrorJson(code, message, extra){
        return '{"error": {"code": "'+code+'","message": "'+message+'","extra": "'+extra+'"}}';
    }
    
    function authenticate(name, pass, fn) {
        // if(name=='ad' )
       if(name=='admin' && pass=='123456') 
        return fn(null, 'admin');
      else 
        return fn(new Error("Authentication failed"));                                                                                                                                                                                                                                                 
    } 
}