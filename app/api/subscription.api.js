
module.exports = function(app,con) {
    app.get('/api/subscription', function(req, res) {
        con.query('SELECT id, name, max_keywords keywords, max_deals deals, max_events events, price price1,6_months_price price2,12_months_price price3, status statusValue, case when status=1 then \'true\' else \'false\' end status FROM subscription_plans',function(err,rows){
            if (!err)
                res.json(rows)
            else
                res.send(err);
        });
    });
    
    app.delete('/api/subscription/:id:status', function(req, res){
        con.query('UPDATE subscription_plans SET status=? WHERE id=?',[req.params.status,req.params.id],function(err,rows){
            if (!err){
                console.log("subscription plan with id : "+req.params.id+" updated to "+req.params.status)
                res.json(rows.affectedRows)
            }
            else{
                console.log("Error while updating subscription plan : "+err);
                res.send(err);
            }
        });
    });
    
}