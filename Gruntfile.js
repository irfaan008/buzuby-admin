module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // uglify: {
    //   options: {
    //     expand:true,
    //     banner: '/*! <%= pkg.name %> This file is autogenerated on <%= grunt.template.today("yyyy-mm-dd hh:MM:ss") %> Modifying this would result in change on next deployment */\n',
    //     mangle: false,
    //     compress: false,
    //     beautify:true
    //   },
    //   build: {
    //     src: ['public/core.js','public/**/*.js','!public/assets/**','!public/login/**'],
    //     dest: 'public/core.min.js'
    //   }
    // },
    concat: {
        options: {
            separator: ';',
            stripBanners: true,
        },
        dist: {
            src: ['public/core.js','public/**/*.js','!public/assets/**','!public/login/**'],
            dest: 'public/core.min.js'
        },
    },
  });

  // Load the plugin that provides the "uglify" task.
//   grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');

  // Default task(s).
//   grunt.registerTask('default', ['uglify']);
// grunt.task.run(['default']);
};