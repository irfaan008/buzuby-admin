

postLoginModule.controller('CustomersCtrl', ['$scope', '$resource', '$location', '$stateParams', 'Flash',
    function($scope, $resource, $location,$stateParams,Flash){
    	var Customers = $resource('/api/customers');
        Customers.query(function(customers){
            $scope.customers = customers;
        });
        
        $scope.delete = function(id,idx){
           console.log("inbuilt delete called: "+id);
           var DeleteHelper = $resource('/api/customers/:id');
            DeleteHelper.delete({id:id}, function(data){
                if(typeof data.error != 'undefined'){
                    Flash.create('danger', data.error.message);   
                }else{
                    console.log("Data deleted id : "+id+" & index:"+idx);
                    var message = '<strong> Success!</strong>  Data deleted successfully';
                    Flash.create('success', message);
                    $scope.customers.splice(idx,1);        
                    
                }
            });
        };
    }]);