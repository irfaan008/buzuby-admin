
postLoginModule.controller('MerchantsCtrl', ['$scope', '$resource', '$location', '$stateParams', 'Flash', 
    function($scope, $resource, $location,$stateParams,Flash){
    	var Merchants = $resource('/api/merchants');
        Merchants.query(function(merchants){
            $scope.merchants = merchants;
        });
       
       $scope.showLocation=function(lat, long){
           window.open("http://maps.google.com/maps?q="+lat+","+long);
       } 
       
       $scope.showDetail=function(id,idx) {
           alert("This functionality is under development")
       }
        
       $scope.delete = function(id,idx){
           console.log("inbuilt delete called: "+id);
           var DeleteHelper = $resource('/api/merchants/:id');
            DeleteHelper.delete({id:id}, function(data){
                if(typeof data.error != 'undefined'){
                    // $location.path('/merchants');
                    Flash.create('danger', data.error.message);
                    // $scope.error=data.error.message;   
                }else{
                    console.log("Data deleted id : "+id+" & index:"+idx);
                    var message = '<strong> Success!</strong>  Data deleted successfully';
                    Flash.create('success', message);
                    $scope.merchants.splice(idx,1);        
                }
            });
        };
    }]);

