var preLoginModule = angular.module('preLoginModule', ['ui.router','ngResource','ngRoute','ngFlash']);

preLoginModule.config(['$stateProvider', '$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
    
  // $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        .state('login', {
            url: '/login/',
            templateUrl: '../login/login.tpl.html',
            controller: 'LoginCtrl'
        })
        
}]);


    
preLoginModule.run(function($state, $rootScope) { 
        $state.go("login");
        $rootScope.$state = $state;
    });