var postLoginModule = angular.module('postLoginModule', ['datatables','ui.router','ngResource','ngRoute','ngFlash']);
// tip : datatables is responsible to convert any tables having attribute datatabels='ng' to modern table
// with paging, sorting, filtering option. See more detail at http://l-lin.github.io/angular-datatables/#/angularWay 
postLoginModule.config(['$stateProvider', '$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
    
  // $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/',
            menu: 'titlehome',
            template: '<h1>This is a home</h1>'
            // templateUrl: '../partials/dashboard.html'
        })
        
        // nested list with custom controller
        .state('customers', {
            url: '/customers',
            templateUrl: '../customer/customer.tpl.html',
            menu: 'menuCustomer',
            controller: 'CustomersCtrl'
        })

        // nested list with just some random string data
        .state('merchants', {
            url: '/merchants',
            templateUrl: '../merchant/merchant.tpl.html',
            menu: 'menuMerchant',
            controller: 'MerchantsCtrl',
        })
        
        .state('subscription', {
            url: '/merchants/subscription',
            templateUrl: '../merchant/merchant.subscription.tpl.html',
            menu: 'menuMerchantSubscription',
            controller: 'MerchantsSubscriptionCtrl',
        })
        
        .state('subscriptionMeta', {
            url: '/subscription',
            templateUrl: '../subscription/subscription.tpl.html',
            menu: 'menuSubscription',
            controller: 'SubscriptionCtrl',
        })
        
        .state('payment', {
            url: '/payments',
            templateUrl: '../payment/payment.tpl.html',
            menu: 'menuPayment',
            controller: 'PaymentCtrl',
        })
        
        .state('login', {
            url: '/admin/',
            templateUrl: '../partials/login.html',
            controller: 'LoginCtrl'
        })
        
        .state('logout', {
            url: '/logout/',
            template: 'Logging out from the system. Please wait..',
            controller: 'LogoutCtrl'
        })
        
}]);

    
postLoginModule.run(function($state, $rootScope) { 
        $state.go("customers");
        $rootScope.$state = $state;
    });

