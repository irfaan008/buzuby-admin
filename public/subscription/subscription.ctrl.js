
postLoginModule.controller('SubscriptionCtrl', ['$scope', '$resource', '$location', '$stateParams', 'Flash', 
    function($scope, $resource, $location,$stateParams,Flash){
    	var Subscription = $resource('/api/subscription');
        Subscription.query(function(plans){
            $scope.items = plans;
        });
        
       $scope.toggleStatus = function(id,status,idx){
           //   toggling status
           status=status==1?0:1;
           console.log("toggleStatus called for id: "+id+" status:"+status);
           var UpdateHelper = $resource('/api/subscription/:id:status');
            UpdateHelper.delete({id:id,status:status}, function(data){
                if(typeof data.error != 'undefined'){
                    Flash.create('danger', data.error.message);
                }else{
                    console.log("Data updated id : "+id+" & status:"+status);
                    var message = '<strong> Success!</strong>  Data updated successfully';
                    Flash.create('success', message);
                    // $scope.items.splice(idx,1);        
                }
            });
       };
    }]);

