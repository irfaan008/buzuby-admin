
preLoginModule.controller('LoginCtrl', ['$scope', '$resource', '$location', 'Flash',
    function($scope, $resource, $window,Flash){
        $scope.login = function () {
          var LoginHelper = $resource('/api/login');
          LoginHelper.save($scope.user,  function (data) {
            console.log("from login controller");
                console.log(data.error);
            if(typeof data.error == 'undefined'){
                    console.log("logged in");
                    window.location.href='/#/';
            }else{
                // $scope.isError=true;
                // $scope.error=data.error;
                Flash.create('danger', data.error.message);
            }
            
          })   
        };     
          
    }]);
    